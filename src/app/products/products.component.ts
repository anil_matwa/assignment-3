import { HttpClient } from '@angular/common/http';
import { OnInit } from '@angular/core';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { IProducts } from 'product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit  {

  constructor(private http: HttpClient,private prodservice: ProductService) {}

  public name: string;
  public quantity: number;
  @Output() objMsg = new EventEmitter<{name: string, quantity: number}>();
  // public addedData =new MatTableDataSource<IProducts>();
  public ad=[];
  
  ngOnInit(): void {
    // this.prodservice.getProduct().subscribe((res) => this.addedData.data = res);
    this.prodservice.getProduct().subscribe((res) => this.ad = res);
  }
  
  displayedColumns: string[] = ['name','quantity'];
  
  addtoTable() {
    var custdto:any = {};
    custdto.name=this.name;
    custdto.quantity=this.quantity;
    this.http.post("http://localhost:3000/products",custdto).subscribe(res =>this.ad)

    this.ngOnInit();
   // this.ad.push( {name:this.name,quantity:this.quantity} );
}
    
  


  sendMsg() {
    this.objMsg.emit({name: this.name,quantity: this.quantity});
  
  }
}

