import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IProducts } from 'product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  private url: string = "http://localhost:3000/products";

  getProduct() {
    return this.http.get<IProducts[]>(this.url);
  }

}
