import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FED3';


  public model = { name: "", quantity: 0 }

  Ifadded: boolean = false;

  display(objMsg)
  {
    this.model=objMsg;
    this.Ifadded=true;
  }

}
